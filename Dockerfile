FROM ubuntu:22.04
RUN apt-get -y update && apt-get -y install nginx
COPY default /etc/nginx/sites-available/default
COPY default /etc/nginx/sites-available/default
COPY index.html /var/www/html
CMD ["/usr/sbin/nginx", "-g", "daemon off;"]